function Human(name, age){
	this.name = name;
	this.age = age;

	this.sayHello = function(){
		document.write(`Hello! My name is ${this.name}!` + '<br>');
	}

	this.sayAge = function(){
		document.write(`I am ${this.age}` + '<br>');
	}
}

Human.create = function(){
	return new Human('John', 23);
}

let human1 = new Human('Bill', 28);
human1.sayHello();
human1.sayAge();

Human.create().sayHello();
Human.create().sayAge();