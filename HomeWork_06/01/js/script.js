let Human = function(name, age){
	this.name = name;
	this.age = age;
}

let humansArr = [
	new Human('Bill', 32),
	new Human('John', 25),
	new Human('Thomas', 38),
	new Human('Harry', 18),
	new Human('Oliver', 45)];

function print(arr){
	for(i=0; i<arr.length; i++){
		document.write(arr[i].name + ', ' + arr[i].age + '<br>');
	}
}

function sortArr(arr){
	//arr.sort((a, b) => a.age > b.age ? 1 : -1);
	arr.sort((a, b) => a.age - b.age);
}

document.write('Not sorted:<br>');
print(humansArr);

sortArr(humansArr);

document.write('<br>Sorted:<br>');
print(humansArr);