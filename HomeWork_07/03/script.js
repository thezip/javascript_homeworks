let mass = ['hello', 'world', 23, '25', null];

function filterBy(arr, type){
	let resMass = [];
	arr.forEach(function(item){
		if((typeof item) != type){
			resMass.push(item);
		}
	});
	return resMass;
}

console.log(filterBy(mass, 'number'));
console.log(filterBy(mass, 'string'));