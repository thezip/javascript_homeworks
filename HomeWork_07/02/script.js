function creareNewUser(){
	let newUser = {};
	
	newUser.firstName = prompt('Введите имя:', '');
	newUser.lastName = prompt('Введите фамилию:', '');
	newUser.birthday = prompt('Введите дату рождения в формате "dd.mm.yyyy"', '');


	newUser.getLogin = function(){
		return (this.firstName[0] + this.lastName).toLowerCase();
	}

	newUser.getAge = function(){
		let date = new Date();
		return date.getFullYear() - +this.birthday.split('.')[2];
	}

	newUser.getPassword = function(){
		return (this.firstName[0].toUpperCase() + 
				this.lastName.toLowerCase() + 
				this.birthday.split('.')[2]);
	}

	return newUser;
}

let user1 = creareNewUser();
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());