document.onkeydown = (e) => {
    e = e || window.event;
    if (e.ctrlKey && e.keyCode == 65) {
    	e.preventDefault();
    	alert('Ctrl + A');
    }

    if (e.ctrlKey && e.shiftKey && e.keyCode == 83) {
    	e.preventDefault();
    	alert('Ctrl + Shift + S');
    }

    if (e.ctrlKey && e.keyCode == 83) {
    	e.preventDefault();
    	alert('Ctrl + S');
    }    
  }
