const btn = document.querySelector('#addBtn');
let k = 0;

btn.onclick = () =>{
	if(k < 10){
		let div = document.createElement('div');
		div.className = 'addDiv';
		div.innerHTML = 'Hello, Java Script!';
		document.body.append(div);
		k++;
	} else{
		let divs = document.querySelectorAll('.addDiv');
		divs.forEach((item) => {
			item.remove();
		});
		k = 0;
	}
}